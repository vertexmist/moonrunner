#Moonrunner

##Dev start!

```
git clone https://gitlab.com/vertexmist/bagofutils
cd bagofutils
mvn clean
mvn install
cd ..

git clone https://gitlab.com/vertexmist/bagofgameutils.git
cd bagofgameutils
mvn clean
mvn install
cd ..

git clone https://gitlab.com/vertexmist/moonrunner.git
cd moonrunner
mvn clean
mvn install

java -jar target/moonrunner-0.1-SNAPSHOT-jar-with-dependencies.jar
```