package org.moonrunner

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.PerspectiveCamera
import com.badlogic.gdx.graphics.VertexAttributes
import com.badlogic.gdx.graphics.g3d.Environment
import com.badlogic.gdx.graphics.g3d.Material
import com.badlogic.gdx.graphics.g3d.Model
import com.badlogic.gdx.graphics.g3d.ModelInstance
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder
import com.badlogic.gdx.math.Vector3
import org.bagofgameutils.layer.Layer3D
import org.bagofgameutils.rendering.RenderingContext3D
import org.bagofutils.entitysystem.Entity


/**
 * Shows a rotating testbox
 */
class TestBoxLayer: Layer3D() {

    lateinit var instance: ModelInstance
    lateinit var model: Model
    lateinit var environment: Environment

    override fun doInit(entity: Entity) {
        // Camera
        camera.position.set(10f, 10f, 10f)
        camera.lookAt(0f, 0f, 0f)
        camera.near = 1f
        camera.far = 300f
        camera.update()

        environment = Environment()
        environment.set(ColorAttribute(ColorAttribute.AmbientLight, 0.4f, 0.4f, 0.4f, 1f))
        environment.add(DirectionalLight().set(0.8f, 0.8f, 0.8f, -1f, -0.8f, -0.2f))


        val modelBuilder = ModelBuilder()
        model = modelBuilder.createBox(5f, 5f, 5f,
                                       Material(ColorAttribute.createDiffuse(Color.GREEN)),
                                       (VertexAttributes.Usage.Position or VertexAttributes.Usage.Normal).toLong())
        instance = ModelInstance(model)
    }

    override fun render(context: RenderingContext3D) {
        (context.camera as PerspectiveCamera).project(Vector3(10f, 10f, 10f), 0f, 0f, Gdx.graphics.width.toFloat(), Gdx.graphics.height.toFloat())
        camera.near = 0.1f
        camera.far = 1000f
        camera.lookAt(0f, 0f, 0f)
        camera.position.set(10f, 10f, 10f)
        camera.update()
        context.camera.update(true)
        context.modelBatch.render(instance, environment)
    }

    override fun doDispose() {
        model.dispose()
    }
}