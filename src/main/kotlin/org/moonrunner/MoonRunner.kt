package org.moonrunner

import com.badlogic.gdx.backends.lwjgl.LwjglApplication
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.VertexAttributes
import com.badlogic.gdx.graphics.g3d.Material
import com.badlogic.gdx.graphics.g3d.Model
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder
import org.bagofgameutils.Game
import org.bagofgameutils.controls.Controllable
import org.bagofgameutils.controls.InputControlProcessor
import org.bagofgameutils.controls.InputMapping
import org.bagofgameutils.controls.controller.InputController
import org.bagofgameutils.layer.LayerProcessor
import org.bagofgameutils.maprenderers.tilemaprenderer.EntitySpaceRenderer3D
import org.bagofgameutils.quitprocessor.QuitProcessor
import org.bagofgameutils.scheduler.ScheduleProcessor
import org.bagofgameutils.space.BruteForceEntitySpace
import org.bagofgameutils.space.EntitySpace
import org.bagofgameutils.space.Location
import org.bagofutils.Symbol
import org.bagofutils.entitysystem.World
import org.bagofutils.geometry.double3.MutableDouble3
import org.bagofutils.random.RandomSequence
import org.moonrunner.appearance.ModelAppearance
import org.moonrunner.camerafocus.CameraFocus
import org.moonrunner.camerafocus.CameraService
import org.moonrunner.capions.CaptionService
import org.moonrunner.physics.Physical
import org.moonrunner.terrain.Terrain
import org.moonrunner.terrain.TerrainLayer


/**
 *
 */
class MoonRunner : Game(gameName, developmentMode = true) {

    val space: EntitySpace = BruteForceEntitySpace()

    lateinit var camService: CameraService


    override fun createProcessors(world: World) {
        // Quit from esc
        camService = world.addProcessor(CameraService())
        world.addProcessor(QuitProcessor())
        world.addProcessor(InputControlProcessor())
        world.addProcessor(ScheduleProcessor())
        world.addProcessor(LayerProcessor())
        world.addProcessor(CaptionService())
       // world.addProcessor(PhysicsProcessor())

        // TODO: Add 3d renderer for the entity space
    }

    override fun setupWorld(world: World) {

        // Testbox
        world.createEntity(TestBoxLayer())


        // Show startup texts
        val captionService = world[CaptionService::class]
        captionService.showText("Welcome", Color.GRAY, preDelay = 3.0, duration = 1.0)
        captionService.showText("To $gameName", duration = 2.0)
        captionService.showText("Get Ready...", duration = 0.5)
        captionService.showText("Go!", duration = 0.35)

        val terrainLayer = TerrainLayer(Terrain({ x, y, time -> Math.sin(x / 10.0 + y / 100.0 + time / 89.0).toFloat() }))
        //terrainLayer.camera = camService.camera
        world.createEntity(terrainLayer)

        val model = createModel(Color.BLUE)

        // Create player
        val bindings = InputMapping()
        bindings.set(Symbol["space"], Symbol["forward"])
        bindings.set(Symbol["W"], Symbol["up"])
        bindings.set(Symbol["S"], Symbol["down"])
        bindings.set(Symbol["A"], Symbol["left"])
        bindings.set(Symbol["D"], Symbol["right"])
        world.createEntity(Location(-10.0, -10.0, -10.0),
                           ModelAppearance(model),
                           CameraFocus(),
                           Controllable("forward", "left", "right", "up", "down"),
                           InputController(bindings),
                           Physical(200.0, 10.0, MutableDouble3(10.0, 0.0, 0.0)))

        // Create space
        world.createEntity(space, EntitySpaceRenderer3D())

        addStardust()
    }


    fun addStardust(num: Int = 100,
                    random: RandomSequence = RandomSequence.default) {

        val model = createModel(Color.RED)

        for (i in 1..num) {
            world.createEntity(Location(random.nextGaussian(0.0, 10.0),
                                        random.nextGaussian(0.0, 10.0),
                                        random.nextGaussian(0.0, 10.0),
                                        space),
                               ModelAppearance(model)
                               )
        }
    }

    private fun createModel(color: Color): Model {
        val model = ModelBuilder().createBox(1f, 1f, 1f,
                                             Material(ColorAttribute.createDiffuse(color)),
                                             (VertexAttributes.Usage.Position or VertexAttributes.Usage.Normal).toLong())
        return model
    }


    companion object {
        val gameName = "Moon Runner"
    }
}


fun main(args: Array<String>) {
    val game = MoonRunner()

    val conf = LwjglApplicationConfiguration()
    conf.width = 1000
    conf.height = 800

    LwjglApplication(game, conf)
}

