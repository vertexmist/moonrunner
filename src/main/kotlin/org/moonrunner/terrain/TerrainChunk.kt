package org.moonrunner.terrain

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.Mesh
import com.badlogic.gdx.graphics.g3d.Environment
import com.badlogic.gdx.graphics.g3d.Material
import com.badlogic.gdx.graphics.g3d.Model
import com.badlogic.gdx.graphics.g3d.ModelInstance
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder
import com.badlogic.gdx.math.Vector3
import org.bagofgameutils.gdxutils.ShapeBuilder
import org.bagofgameutils.rendering.RenderingContext3D
import org.bagofutils.geometry.int2.Int2
import org.bagofutils.time.Time

/**
 *
 */
class TerrainChunk(val terrain: Terrain,
                   val gridPos: Int2,
                   val size: Int = 64,
                   val chunkSize_meter: Float = 10f) {

    private val shapeBuilder = ShapeBuilder()
    private val modelBuilder = ModelBuilder()

    private lateinit var mesh: Mesh
    private lateinit var model: Model
    private lateinit var modelInstance: ModelInstance
    private var blockMaterial = Material(ColorAttribute.createDiffuse(Color.GREEN))


    fun init() {
        // Create vertexes
        val pos = Vector3()
        val xOffs = gridPos.x * chunkSize_meter
        val zOffs = gridPos.y * chunkSize_meter
        val cellSize = chunkSize_meter / size
        for (z in 0..size) {
            for (x in 0..size) {
                val xp = xOffs + x * cellSize
                val zp = zOffs + z * cellSize
                pos.set(xp, 0f, zp)
                shapeBuilder.addVertex(pos)
            }
        }

        // Create faces
        var index = size+1
        for (z in 1..size) {
            for (x in 1..size) {
                shapeBuilder.addQuad((index - 1 - size).toShort(),
                                     (index - size).toShort(),
                                     (index - 1).toShort(),
                                     (index ).toShort(),
                                     true)
                index++
            }
        }

        mesh = shapeBuilder.createMesh(false)
        modelBuilder.begin()
        modelBuilder.part("chunk", mesh, GL20.GL_TRIANGLES, blockMaterial)
        model = modelBuilder.end()
        modelInstance = ModelInstance(model)

    }

    fun update(time: Time) {
        // TODO
    }

    fun render(context: RenderingContext3D,
               environment: Environment) {
        context.modelBatch.render(modelInstance, environment)
    }
}