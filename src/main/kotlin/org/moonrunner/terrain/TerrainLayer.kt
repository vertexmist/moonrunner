package org.moonrunner.terrain

import com.badlogic.gdx.graphics.g3d.Environment
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight
import org.bagofgameutils.layer.Layer3D
import org.bagofgameutils.rendering.RenderingContext3D
import org.bagofutils.entitysystem.Entity
import org.bagofutils.geometry.double2.ImmutableDouble2
import org.bagofutils.geometry.double2.MutableDouble2
import org.bagofutils.geometry.int2.Int2
import org.bagofutils.geometry.int2.MutableInt2

/**
 *
 */
class TerrainLayer(val terrain: Terrain,
                   val chunkGridSize: Int = 64,
                   val chunkSize_meter: Float = 100f,
                   val chunkLoadDistance: Int = 4,
                   val chunkUnloadDistance: Int = chunkLoadDistance + 1): Layer3D() {

    private val chunkGrid = ImmutableDouble2(chunkSize_meter.toDouble(),
                                             chunkSize_meter.toDouble())

    private val chunks: MutableMap<Int2, TerrainChunk> = HashMap()

    private val cameraPos2D = MutableDouble2()
    private val cameraChunkPos = MutableInt2()
    private val chunkPos = MutableInt2()
    private lateinit var environment: Environment

    override fun doInit(entity: Entity) {
        clearDepthBuffer = true
        clearStencilBuffer = true

        // Camera
        camera.position.set(10f, 10f, 10f)
        camera.lookAt(0f, 0f, 0f)
        camera.near = 1f
        camera.far = 300f
        camera.update()

        environment = Environment()
        environment.set(ColorAttribute(ColorAttribute.AmbientLight, 0.4f, 0.4f, 0.4f, 1f))
        environment.add(DirectionalLight().set(0.8f, 0.8f, 0.8f, -1f, -0.8f, -0.2f))


    }

    override fun render(context: RenderingContext3D) {
        // Find camera pos
        val pos = context.camera.position
        cameraPos2D.set(pos.x.toDouble(), pos.z.toDouble())
        cameraPos2D.floorDiv(chunkGrid, out = cameraChunkPos)

        // Unload chunks that are far enough away
        for ((pos, chunk) in chunks) {
            if (Math.abs(pos.x - cameraChunkPos.x) > chunkUnloadDistance) {
                println("Removed chunk $pos")
                chunks.remove(pos)
            }
        }

        // Create chunks that are close enough to the camera, delete chunks outside camera range + hysteresis threshold
        for (z in cameraChunkPos.y - chunkLoadDistance .. cameraChunkPos.y + chunkLoadDistance) {
            for (x in cameraChunkPos.x - chunkLoadDistance .. cameraChunkPos.x + chunkLoadDistance) {
                chunkPos.set(x, z)
                // Load if needed
                chunks.getOrPut(chunkPos, {
                    val chunk = TerrainChunk(terrain,
                                             MutableInt2(chunkPos),
                                             chunkGridSize,
                                             chunkSize_meter)
                    chunk.init()
                    println("Created chunk $chunkPos")
                    chunk
                })
            }
        }

        // Update chunks
        val time = entity!!.world.time
        for (chunk in chunks.values) {
            chunk.update(time)
        }

        // Render chunks
        for (chunk in chunks.values) {
            chunk.render(context, environment)
        }

    }

    override fun doDispose() {
    }

}