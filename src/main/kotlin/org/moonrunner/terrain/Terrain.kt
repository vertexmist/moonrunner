package org.moonrunner.terrain

/**
 * Contains info about a terrain that can be rendered
 */
class Terrain(var heightFunction: (x: Float, y: Float, time: Float) -> Float) {

    fun getHeight(x: Float, y: Float, time: Float): Float = heightFunction(x, y, time)

}