package org.moonrunner

import org.bagofgameutils.space.Location
import org.bagofutils.entitysystem.Entity
import org.bagofutils.entitysystem.entitymanager.ComponentRef
import org.bagofutils.entitysystem.processor.EntityProcessorBase
import org.bagofutils.geometry.double3.MutableDouble3
import org.bagofutils.time.Time
import org.bagofutils.updating.strategies.VariableTimestepStrategy
import org.moonrunner.physics.Physical

/**
 *
 */
class PhysicsProcessor: EntityProcessorBase(VariableTimestepStrategy(),
                                            Physical::class, Location::class) {

    private val physicalRef = ComponentRef(Physical::class)
    private val locationRef = ComponentRef(Location::class)

    private val tempPos = MutableDouble3()

    override fun updateEntity(entity: Entity, time: Time) {
        val dt = time.stepDurationSeconds
        val physical = physicalRef[entity]
        val location = locationRef[entity]

        tempPos.set(location)

        if (physical.mass > 0) {
            // Apply friction
            physical.acc.addScaled(physical.velocity, -(airResistanceCoefficient * physical.radius / physical.mass))

            // Update velocity from forces
            physical.velocity.addScaled(physical.acc, dt / physical.mass)
        }

        // Apply vel
        tempPos.addScaled(physical.velocity, dt)

        // Reset thrust
        physical.acc.zero()

        // Set pos
        location.setPosition(tempPos)
    }

    companion object {
        // See https://www.gamedev.net/forums/topic/601818-air-resistance/
        val airResistanceCoefficient: Double  = 6.0 * Math.PI * 18.6 * 0.000001
    }
}