package org.moonrunner.physics

import com.badlogic.gdx.math.Quaternion
import org.bagofutils.entitysystem.Component
import org.bagofutils.geometry.double3.MutableDouble3

/**
 *
 */
class Physical(var mass: Double = 1.0,
               var radius: Double = 1.0,
               var velocity: MutableDouble3 = MutableDouble3(),
               var acc: MutableDouble3 = MutableDouble3(),
               var rotation: Quaternion = Quaternion()): Component {
}