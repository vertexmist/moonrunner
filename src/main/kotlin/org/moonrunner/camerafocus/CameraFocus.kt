package org.moonrunner.camerafocus

import org.bagofutils.entitysystem.Component

/**
 * Tags an entity as the focus of the camera.
 */
class CameraFocus: Component {

}