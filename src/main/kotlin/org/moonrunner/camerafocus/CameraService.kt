package org.moonrunner.camerafocus

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.PerspectiveCamera
import org.bagofgameutils.space.Location
import org.bagofutils.entitysystem.Entity
import org.bagofutils.entitysystem.World
import org.bagofutils.entitysystem.entitygroup.EntityGroup
import org.bagofutils.entitysystem.entitymanager.ComponentRef
import org.bagofutils.entitysystem.processor.EntityProcessorBase
import org.bagofutils.time.Time
import org.bagofutils.updating.strategies.VariableTimestepStrategy

/**
 *
 */
class CameraService(): EntityProcessorBase(VariableTimestepStrategy(),
                                           CameraFocus::class, Location::class) {

    val camera: PerspectiveCamera = PerspectiveCamera()

    private val locationRef = ComponentRef(Location::class)

    override fun doInit(world: World, entities: EntityGroup) {
        updateCamera()
    }

    override fun preUpdate(time: Time) {
    }

    override fun updateEntity(entity: Entity, time: Time) {
        val location = locationRef[entity]
        camera.position.x = location.x.toFloat()
        camera.position.y = location.y.toFloat()
        camera.position.z = location.z.toFloat()
    }

    override fun postUpdate(time: Time) {
        updateCamera()
    }

    private fun updateCamera() {
        camera.viewportWidth = Gdx.graphics.width.toFloat()
        camera.viewportHeight = Gdx.graphics.height.toFloat()
        camera.near = 1f
        camera.far = 1000f
        camera.update()
    }
}