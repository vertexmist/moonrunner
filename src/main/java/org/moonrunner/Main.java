package org.moonrunner;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import org.bagofgameutils.Game;

/**
 *
 */
public class Main {
    public static void main(String[] args) {
        Game game = new MoonRunner();

        LwjglApplicationConfiguration conf = new LwjglApplicationConfiguration();
        conf.width = 1000;
        conf.height = 800;

        new LwjglApplication(game, conf);
    }
}
